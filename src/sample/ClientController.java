package sample;

import fr.fx.dao.ClientImpl;
import fr.fx.dao.IClient;
import fr.fx.entities.Client;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class ClientController implements Initializable {

    @FXML
    private TextField idTxt;
    @FXML
    private TextField nomTxt;
    @FXML
    private TextField prenomTxt;
    @FXML
    private TextField adressTxt;
    private IClient clientdao= new ClientImpl();
    public void addClient(ActionEvent event){
        if (nomTxt.getText()!=null || prenomTxt.getText()!=null || adressTxt.getText()!=null){
            int id=Integer.parseInt(idTxt.getText());
           int ok= clientdao.add(new Client(id,nomTxt.getText(),prenomTxt.getText(),adressTxt.getText()));
           if (ok==1){
               System.out.println("bakhna");
           }
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
