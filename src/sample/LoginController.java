package sample;

import fr.fx.dao.AdminImpl;
import fr.fx.dao.IAdmin;
import fr.fx.entities.Admin;
import fr.fx.entities.Client;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import javafx.event.ActionEvent;
import javafx.scene.layout.AnchorPane;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    @FXML
    private TextField logintxt;
    @FXML
    private TextField passwordtxt;
    @FXML
    private Button btnConnection;
    @FXML
    private AnchorPane anchorPane;
    private List<Client> list= new ArrayList<>();
    private IAdmin admindao= new AdminImpl();
    private Admin admin= new Admin();
    public void getConnection(ActionEvent event) throws IOException {
       if (logintxt.getText()!=null || passwordtxt.getText()!=null){
           admin= admindao.getConnection(logintxt.getText(),passwordtxt.getText());
           if (admin.getEmail()!=null){
              AnchorPane pane = FXMLLoader.load(getClass().getResource("client.fxml"));
              anchorPane.getChildren().setAll(pane);
           }
           else {

           }
       }
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
