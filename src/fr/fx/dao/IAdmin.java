package fr.fx.dao;

import fr.fx.entities.Admin;

public interface IAdmin {
    Admin getConnection(String login, String password);
}
