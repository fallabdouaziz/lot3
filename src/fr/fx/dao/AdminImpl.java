package fr.fx.dao;

import fr.fx.entities.Admin;
import java.sql.ResultSet;

public class AdminImpl implements IAdmin {
    private DB db= new DB();
    private ResultSet rs;
    private int ok;
    @Override
    public Admin getConnection(String login, String password) {
        String query= "Select *from admin where email=? and password=?";
        Admin admin= new Admin();
        try{
            db.initPrepare(query);
            db.getPstm().setString(1,login);
            db.getPstm().setString(2,password);
            rs=db.executeSelect();
            while (rs.next()){
                admin.setId(rs.getInt(1));
                admin.setNom(rs.getString(2));
                admin.setPrenom(rs.getString(3));
                admin.setEmail(rs.getString(4));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return admin;
    }
}
