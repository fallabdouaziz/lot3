package fr.fx.dao;


import fr.fx.entities.Client;

import java.util.List;

public interface IClient {
     int add(Client client);
     List<Client> listClient();
     int update(Client client);
     Client findById(int id);
}
