package fr.fx.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DB {
    //Pour la connexion
    private Connection cnx;
    //Pour les rrésultats de requettes de type Select
    private ResultSet resultSet;
    //our les requettes préparées
    private PreparedStatement pstm;
    //pour les MAJ
    private int ok;

    private void getConnection(){
        String url="jdbc:mysql://localhost:3306/coursUtilisationIde?useTimeZone=true&serverTimezone=UTC";
        String user= "root";
        String password="";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            cnx= DriverManager.getConnection(url,user,password);

        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void initPrepare(String query){
        try {
            getConnection();
            pstm= cnx.prepareStatement(query);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public ResultSet executeSelect(){
        try {
            resultSet = pstm.executeQuery();
        }catch (Exception e){
            e.printStackTrace();
        }
        return resultSet;
    }
    public int executeMaj(){
        try{
            ok= pstm.executeUpdate();

        }catch (Exception e){
            e.printStackTrace();
        }
        return ok;
    }
    public void closeConnection(){
        try {
            if (cnx != null){
                cnx.close();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public PreparedStatement getPstm() {
        return pstm;
    }
}
