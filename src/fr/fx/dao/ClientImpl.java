package fr.fx.dao;


import fr.fx.entities.Client;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ClientImpl implements IClient {
    private DB db= new DB();
    ResultSet rs;
    private int ok=0;
    @Override
    public int add(Client client) {
        String sql="INSERT INTO client values(?,?,?,?)";
        try {
            db.initPrepare(sql);
            db.getPstm().setInt(1,client.getId());
            db.getPstm().setString(2,client.getNom());
            db.getPstm().setString(3,client.getPrenom());
            db.getPstm().setString(4,client.getAdresse());
            ok=db.executeMaj();
        }catch (Exception e){
            e.printStackTrace();
        }
        return ok;
    }

    @Override
    public List<Client> listClient() {
        List<Client> clientList= new ArrayList<>();
        String sql= "SELECT *FROM client";
        try {
            db.initPrepare(sql);
           rs= db.executeSelect();
           while (rs.next()){
               Client client= new Client();
               client.setId(rs.getInt(1));
               client.setNom(rs.getString(2));
               client.setPrenom(rs.getString(3));
               client.setAdresse(rs.getString(4));
               clientList.add(client);
           }
        }catch (Exception e){
            e.printStackTrace();
        }
        return clientList;
    }

    @Override
    public int update(Client client) {
        return 0;
    }

    @Override
    public Client findById(int id) {
        String query= "Select *from client where id=?";
        Client client= new Client();
        try{
            db.initPrepare(query);
            db.getPstm().setInt(1,id);
            rs=db.executeSelect();
            while (rs.next()){
                client.setId(rs.getInt(1));
                client.setNom(rs.getString(2));
                client.setPrenom(rs.getString(3));
                client.setAdresse(rs.getString(4));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return client;
    }
}
